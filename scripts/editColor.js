const fs = require("fs").promises;
const path = require("path");

const fromTxtFile = "./store/colors.txt";
const toJsonFile= "./store/colors.json";

const resolver = x => path.resolve(x)

const readFile = async (filepath) => {
  try {
    const file = resolver(filepath);
    // const stat = await fs.statSync(file).isFile();
    const data = await fs.readFile(file);
    return data.toString();
  } catch (e) {
    throw new Error(`Got an error typing to read the file ${e.message} `);
  }
};

const writeToFile = async(data,filePath=toJsonFile) => {
    try{
        const file = resolver(filePath);
        await fs.writeFile(file,data)
    }catch(e){
        throw new Error(`Got an error typing to write the file ${e.message} `);
    }
}

readFile(fromTxtFile)
  .then(
    (data) => data.split("\n")
      .map((e) => e.replace("\r", ""))
      .filter((i) => i !== " " && i !== "")
    //   .filter(x => x.match(/[a-z]+$/))
      .filter(x => x.match(/#{1}/))
    //   .reduce((acc, item, pos) => {
    //     // console.log(item.match(/[0-9A-Fa-f]{6}/g))
    //     let second = item.match(/[a-z]+$/);
    //     return [...acc,second]
    //   },[])
      .reduce((acc,item,pos) => {

        return {...acc,[pos]:item}
      },{})

  )
  .then(data => JSON.stringify(data,null,2))
  .then(data => writeToFile(data))
//   .then((data) => console.log(data))
  .catch((err) => console.log(err));
