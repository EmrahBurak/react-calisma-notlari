import React from "react";

function CustomTextInput() {
  const textInputRef = React.useRef();

  const focusTextInput = () => {
    textInputRef.current.focus();
  };

  return (
    <div>
      <input type="text" ref={textInputRef} />

      <input
        type="button"
        value="Focus the text input"
        onClick={focusTextInput}
      />
    </div>
  );
}

export default CustomTextInput;
