import React, { useState } from "react";
import { Button, Popup } from "semantic-ui-react";


import styles from './popup.module.css'

function PopupExampleContextControlled() {
  const contextRef = React.useRef();
  const [open, setOpen] = useState(false);

  return (
    <>
    <div className={styles.body}>
      <Button
        content="Open Controelled Popup"
        onClick={() => setOpen((prevOpen) => !prevOpen)}
      />
      <Popup
        context={contextRef}
        content="Hello"
        position="top center"
        open={open}
      />
      ----------------- 
      <strong ref={contextRef}>here</strong>

    </div>
    </>
  );
}

export default PopupExampleContextControlled;
