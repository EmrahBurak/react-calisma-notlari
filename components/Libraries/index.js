import React,{useEffect,useState} from 'react'

import styles from './lib.module.css'

export default function Library(props) {
    const {title,url}  = props;
    const [colors,setColors] = useState({});
    const [errors, setErrors] = useState("")

    useEffect(async () => {
            await fetch("../../store/colors.json")
                .then(response => response.json())
                .then(data => setColors(data))
                .then(data => console.log(data))
                .catch(err =>setErrors(prevErr => err.message))
    },[])


    const getRandomColor = () => {
    }



  return (
    <div className={styles.lib} >
        <a href={url} target="_blank" rel="noopener nereferrer">{title}</a>
    </div>
  )
}
