import CustomTextInput from "../components/CustomTextInput";
import PopupExampleContextControlled from "../components/PopupExampleContextControlled";

export default {
    "title": "ReactRef/useRef",
}

export const Popup = () => <PopupExampleContextControlled/>

export const Focus = () => <CustomTextInput/>


